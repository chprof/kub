const fs = require('fs');
const components = require('./components');


const sortFiles = () => {
  const newComponents = components.filter(component => component.ignore);
  const guideHTML = fs.readFileSync('./views/guide.html', 'utf-8');
  newComponents.forEach(component => {
    fs.unlink(`../src/scripts/components/${component.name}.js`, () => {})
    fs.unlink(`../src/styles/components/_${component.name}.scss`, () => {})
    fs.unlink(`../src/partials/components/${component.name}.html`, () => {})
  })
  fs.writeFile('../src/styles/_components.scss', '', () => {});
  fs.writeFile('../src/scripts/components.js', '', () => {});
  fs.writeFile('../src/views/guide.html', guideHTML, () => {});
};


sortFiles()