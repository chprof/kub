const fs = require('fs');
const path = require('path');
const vars = require('./vars');
const components = require('./components');

// console.log(vars);
// console.log(components);

// const scssFileConfig = {
// 	name: '_config.scss',
// 	path: ''
// };

// const createNavList = (component) => {
// 	return `<li class="nav-item"><a class="nav-link text-white" href="#${component.name}">${component.name}</a></li>\n`;
// };
// const createContentList = (component) => {
// 	return `<div class="section section--fullpage" id="${component.name}">
// 												<div class="section__in">
// 													<div class="container-fluid">
// 														<h2 class="section__title-guide">${component.name}</h2>
// 														<div>@@include('./src/partials/guide/${component.name}.html')</div>
// 													</div>
// 												</div>
// 											</div>`;
// };
// const createComponentsScssListToImport = (component) => {
// 	return `@import './components/${component.name}';\n`;
// };

// const createGuideFiles = () => {
// 	const newComponents = components.filter(component => !component.ignore);
// 	const guideHTML = fs.readFileSync('./views/guide.html', 'utf-8');
// 	let totalNavStr = '';
// 	let totalContentStr = '';
// 	let scssComponentsStr = '';
// 	newComponents.forEach(component => {
// 		totalNavStr += createNavList(component);
// 		totalContentStr += createContentList(component);
// 		scssComponentsStr += createComponentsScssListToImport(component);
// 		if(!fs.existsSync(`../src/partials/guide/${component.name}.html`)) {
// 			fs.copyFile(component.template.html, `../src/partials/guide/${component.name}.html`, () => {});
// 		}
// 		if(!fs.existsSync(`../src/styles/components/_${component.name}.scss`)) {
// 			fs.copyFile(component.template.scss, `../src/styles/components/_${component.name}.scss`, () => {});
// 		}
// 		if(!fs.existsSync(`../src/scripts/components/${component.name}.js`)) {
// 			fs.copyFile(component.template.js, `../src/scripts/components/${component.name}.js`, () => {});
// 		}
// 	});

// 	let newGuideHTML =  guideHTML.replace(/@@nav/g, totalNavStr).replace(/@@content/g, totalContentStr);
// 	fs.writeFile('../src/views/guide.html', newGuideHTML, () => {});
// 	fs.writeFile('../src/styles/_components.scss', scssComponentsStr, () => {});
// };

// const createSCSSConfigStr = (group, str) => {
// 	str += `//${group.name}\n//============\n\n`;
// 	if(group.name != 'optionsConfig' ) {
// 		group.options.forEach(option => {
// 			const optionArr = Object.entries(option);
// 			const key = optionArr[0][0];
// 			const value = optionArr[0][1];
// 			str += `${key}: ${value};\n`;
// 		})
// 	} else {
// 		const config = group.options;
// 		const convertedStr = '$config: ' + JSON.stringify(config).replace(/\[/g, '(').replace(/\]/g, ')').replace(/\{/g, '(').replace(/\}/g, ')');
// 		str += convertedStr + '\n';
// 	}
// 	str += '\n//============\n//============\n\n';
// 	return str;
// };

// const createSassConfig = () => {
// 	let str = '';
// 	vars.forEach(group => {
// 		str = createSCSSConfigStr(group, str);
// 	})
// 	fs.writeFile(scssFileConfig.name, str, (err) => {
// 		if (err) throw err;
// 		console.log('scss file writed');
// 	});
// };


// const setComponentsDependencies = () => {
// 	console.log(components);
// };

// createSassConfig();
// setComponentsDependencies();
// createGuideFiles();
// injectComponentConfig();



















// const createFoldres = () => {
// 	if (!fs.existsSync(`${__dirname}/components`)) {
// 		fs.mkdir(path.join(__dirname, 'components'), err => {
// 			if(err) {
// 				throw err;
// 			}
// 			console.log('folder components created');
// 		})
// 	}
// 	components.forEach(component => {
// 		const filePath = path.join(__dirname, 'components', `${component.name}.html`)
// 		if (component.ignore != true) {
// 			fs.writeFile(filePath, component.default, err => {
// 				if (err) {
// 					throw err
// 				}
// 				console.log('Component files created');
// 			})
// 		}
// 	})
// };


// const updateGuide  = () => {
// 	fs.readFile(path.join(`${__dirname}/guide.html`), 'utf8', (err, data) => {
// 		if(err) {
// 			throw err;
// 		}
// 		const nav = /@@anchor-nav/g
// 		const section = /@@anchor-section/g
// 		let navItems = ` `;
// 		let sections = ` `;
// 		components.forEach(component => {
// 			navItems += `<li class="nav-item"><a class="nav-link text-white" href="#${component.name}">${component.name}</a></li>\n`
// 		})
// 		components.forEach(component => {
// 			sections += `<div class="section section--fullpage" id="${component.name}">
// 											<div class="section__in">
// 												<div class="container-fluid">
// 													<h2>${component.name}</h2>
// 													<br><hr><br>
// 													@@include('./src/partials/${component.name}.html')
// 												</div>
// 											</div>
// 										</div>\n`
// 		})
// 		let newData = data.replace(nav, navItems).replace(section, sections)
// 		fs.writeFile(path.join(`${__dirname}/new-guide.html`), newData, err => {
// 			if(err) {
// 				throw err;
// 			}
// 		})
// 	})
// }

// createFoldres()
// updateGuide()





// module.exports.createFolders = createFoldres;