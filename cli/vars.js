const fontSizeRelative = 16,
	$xs = 0,
	$sm = 576,
	$md = 768,
	$lg = 992,
	$xl = 1200,
	$xxl = 1400;

	
module.exports = [
	{
		name: 'media',
		options: [
			{ '$xs': 0 },
			{ '$sm': 576 },
			{ '$md': 768 },
			{ '$lg': 992 },
			{ '$xl': 1200 },
			{ '$xxl': 1400 },
		]
	},
	{
		name: 'global',
		options: [
			{'$font': '\'IBM Plex Sans\''},
			{'$color': '#000'},
			{'$linkColor': '#73b8e2'},
			{'$backgroundColor': '#fff'},
			// #f7f7f7
			{'$borderColor': '#e6e6e6'},
			// #c9c9c9
			{'$fontSizeRelative': fontSizeRelative},
			{'$fontSize': '1rem'},
			{'$isBootstrap': true},
		]
	},
	{
		name: 'optionsConfig',
		options: [
			{
				name: 'grid',
				selector: "container",
				options: [
					{
						selector: '',
						value: '1320px',
						gutters: ''
					},
				]
			},
			{
				name: 'font-family',
				selector: "ff",
				options: [
					{
						name: 'IBM Plex Sans',
						src: '../fonts/ibm-plex-sans/ibm-plex-sans',
						options: [
							{
								weight: 100,
								style: 'normal'
							},
							{
								weight: 100,
								style: 'italic'
							},
							{
								weight: 200,
								style: 'normal'
							},
							{
								weight: 200,
								style: 'italic'
							},
							{
								weight: 300,
								style: 'normal'
							},
							{
								weight: 300,
								style: 'italic'
							},
							{
								weight: 400,
								style: 'normal'
							},
							{
								weight: 400,
								style: 'italic'
							},
							{
								weight: 500,
								style: 'normal'
							},
							{
								weight: 500,
								style: 'italic'
							},
							{
								weight: 600,
								style: 'normal'
							},
							{
								weight: 600,
								style: 'italic'
							},
							{
								weight: 700,
								style: 'normal'
							},
							{
								weight: 700,
								style: 'italic'
							},
						]
					}
				]
			},
			{
				name: 'font-size-root',
				selector: "fzr",
				options: [
					{
						mediaString: 'xs',
						media: $xs,
						value: '13px'
					},
					{
						mediaString: 'md',
						media: $md,
						value: '15px'
					},
					{
						mediaString: 'xxl',
						media: $xxl,
						value: '16px'
					},
				]
			},
			{
				name: 'font-size-title',
				selector: "h",
				options: [
					{ '1': 36 / fontSizeRelative + 'rem' },
					{ '2': 32 / fontSizeRelative + 'rem' },
					{ '3': 25 / fontSizeRelative + 'rem' },
					{ '4': 20 / fontSizeRelative + 'rem' },
					{ '5': 18 / fontSizeRelative + 'rem' },
					{ '6': 16 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'font-size-text',
				selector: "text",
				options: [
					{ '1': 20 / fontSizeRelative + 'rem' },
					{ '2': 18 / fontSizeRelative + 'rem' },
					{ '3': 17 / fontSizeRelative + 'rem' },
					{ '4': 16 / fontSizeRelative + 'rem' },
					{ '5': 14 / fontSizeRelative + 'rem' },
					{ '6': 12 / fontSizeRelative + 'rem' },
				]
			},
			{
				name: 'font-size-icon',
				selector: "icon",
				options: [
					{ '1': 25 / fontSizeRelative + 'rem' },
					{ '2': 20 / fontSizeRelative + 'rem' },
					{ '3': 16 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'border-radius',
				selector: "bdrs",
				options: [
					{ '0': 0 / fontSizeRelative + 'rem' },
					{ '1': 50 + '%' },
					{ '2': 3 / fontSizeRelative + 'rem' },
					{ '3': 5 / fontSizeRelative + 'rem' }
				]
			},
			{
				name: 'colors',
				selector: "cf",
				options: [
					{
						name: 'dark',
						selector: "0",
						options: [
							{ '0': '#000' },
							{ '1': '#333' },
							{ '2': '#414141' },
							{ '3': '#383838' },
							{ '4': 'rgba(65, 65, 65, 0.8)' },
						]
					},
					{
						name: 'white',
						selector: "1",
						options: [
							{ '0': '#fff' },
						]
					},
					{
						name: 'gray',
						selector: "2",
						options: [
							{ '0': '#777' },
							{ '1': '#999' },
							{ '2': '#e5e5e5' },
							{ '3': 'rgba(119, 119, 119, 0.502)' }
						]
					},
					{
						name: 'blue',
						selector: "3",
						options: [
							{ '0': '#73b8e2' },
							{ '1': '#508cae' },
							{ '2': '#4a7b9a' },
						]
					},
					{
						name: 'yellow',
						selector: "4",
						options: [
							{ '0': '#efa300' }
						]
					},
					{
						name: 'red',
						selector: "5",
						options: [
							{'0': '#934545'},
						]
					},
				]
			},
			{
				name: 'background-color',
				selector: "bgc",
				options: [
					{
						name: 'white',
						selector: "0",
						options: [
							{'0': '#fff'},
						]
					},
					{
						name: 'gray',
						selector: "1",
						options: [
							{'0': '#f7f7f7'},
						]
					},
					{
						name: 'blue',
						selector: "2",
						options: [
							{'0': '#73b8e2'},
							{'1': '#508cae'},
							{'2': '#497897'}
						]
					},
					{
						name: 'dark',
						selector: "3",
						options: [
							{'0': '#000'},
						]
					},
					{
						name: 'red',
						selector: "4",
						options: [
							{'0': '#934545'},
						]
					},
				]
			},
			{
				name: 'border-color',
				selector: "bdc",
				options: [
					{
						name: 'gray',
						selector: "0",
						options: [
							{'0': '#e6e6e6'},
							{'1': '#c9c9c9'},
							{'2': '#f7f7f7'},
						]
					},
					{
						name: 'blue',
						selector: "1",
						options: [
							{'0': '#73b8e2'},
						]
					},
					{
						name: 'red',
						selector: "2",
						options: [
							{'0': '#934545'},
						]
					},
				]
			},
		]
	}
];